# Akvelonians

| Platform | Build Status                                                                                                                             |
| -------: | ---------------------------------------------------------------------------------------------------------------------------------------- |
| Android  | [![Build status](https://build.appcenter.ms/v0.1/apps/d9db3a7c-775b-4eb3-9976-4706975cbf62/branches/master/badge)](https://appcenter.ms) |
| iOS      | [![Build status](https://build.appcenter.ms/v0.1/apps/45f7dd8e-db82-47ff-a263-0ed25be7d541/branches/master/badge)](https://appcenter.ms) |