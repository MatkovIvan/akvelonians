import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import reducer from './reducers'
import saga from './sagas'
import { loggerMiddleware, navigationMiddleware } from './helpers/redux'
import AppNavigator from './AppNavigator'

const sagaMiddleware = createSagaMiddleware()
const store = createStore(reducer, applyMiddleware(
  loggerMiddleware,
  navigationMiddleware,
  sagaMiddleware
))
sagaMiddleware.run(saga)

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <AppNavigator />
      </Provider>
    );
  }
}
