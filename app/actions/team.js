export const TEAM_ADD_MEMBER = 'TEAM_ADD_MEMBER'
export const TEAM_REMOVE_MEMBER = 'TEAM_REMOVE_MEMBER'


export function teamAddMember(memberId) {
  return { type: TEAM_ADD_MEMBER, memberId: memberId };
}

export function teamRemoveMember(memberId) {
  return { type: TEAM_REMOVE_MEMBER, memberId: memberId };
}
