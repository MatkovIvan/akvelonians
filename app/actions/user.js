export const USER_LOGIN_REQUESTED = 'USER_LOGIN_REQUESTED'
export const USER_LOGIN_SUCCEEDED = 'USER_LOGIN_SUCCEEDED'
export const USER_LOGIN_FAILED = 'USER_LOGIN_FAILED'
export const USER_PROFILE_UPDATED = 'USER_PROFILE_UPDATED'
export const USER_LOGOUT = 'USER_LOGOUT'


export function userLogin(username, password) {
  return { type: USER_LOGIN_REQUESTED, data: { username, password } };
}

export function userLogout() {
  return { type: USER_LOGOUT };
}
