import { createReactNavigationReduxMiddleware, createReduxBoundAddListener } from 'react-navigation-redux-helpers'

export const loggerMiddleware = store => next => action => {
  console.log('Dispatch action:', action.type);
  return next(action);
}
export const navigationMiddleware = createReactNavigationReduxMiddleware('root', state => state.navigation)
export const addListener = createReduxBoundAddListener('root')
