import { combineReducers } from 'redux'
import navigation from './navigation'
import people from './people'
import team from './team'
import user from './user'

export default combineReducers({
  navigation,
  people,
  team,
  user
})
