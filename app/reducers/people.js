import {
  PEOPLE_FETCH_SUCCEEDED,
  USER_LOGOUT
} from '../actions'

export default (state = [], action) => {
  switch (action.type) {
    case PEOPLE_FETCH_SUCCEEDED:
      return action.people;
    case USER_LOGOUT:
      return [];
    default:
      return state;
  }
}
