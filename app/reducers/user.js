import {
  USER_LOGIN_REQUESTED,
  USER_LOGIN_SUCCEEDED,
  USER_LOGIN_FAILED,
  USER_PROFILE_UPDATED,
  USER_LOGOUT
} from '../actions'

export default (state = null, action) => {
  switch (action.type) {
    case USER_LOGIN_REQUESTED:
    return { loading: true };
    case USER_LOGIN_SUCCEEDED:
      return action.user;
    case USER_LOGIN_FAILED:
      const { message } = action;
      return { error: { message }};
    case USER_PROFILE_UPDATED:
      const { profile } = action;
      return { ...state, profile };
    case USER_LOGOUT:
      return null;
    default:
      return state;
  }
}
