import { AppNavigator } from '../AppNavigator'

const loginAction = AppNavigator.router.getActionForPathAndParams('Login');
const initialState = AppNavigator.router.getStateForAction(loginAction);
export default (state = initialState, action) => {
  return AppNavigator.router.getStateForAction(action, state) || state;
}
