import {
  TEAM_ADD_MEMBER,
  TEAM_REMOVE_MEMBER,
  USER_LOGOUT
} from '../actions'

export default (state = [], action) => {
  let id, index;
  switch (action.type) {
    case TEAM_ADD_MEMBER:
      id = action.memberId;
      index = state.indexOf(id);
      if (index === -1) {
        return [ ...state, id ];
      }
      return state;
    case TEAM_REMOVE_MEMBER:
      id = action.memberId;
      index = state.indexOf(id);
      if (index !== -1) {
        return state.filter((array, i) => i !== index);
      }
      return state;
    case USER_LOGOUT:
      return [];
    default:
      return state;
  }
}
