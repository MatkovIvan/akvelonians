import Base64 from 'base-64'

const baseURL = 'https://prism.akvelon.net:8441/';

async function prismFetch(route, options = {}) {
  const response = await fetch(`${baseURL}${route}`, {
    ...options,
    credentials: 'include'
  });
  const responseJson = await response.json();
  if (response.status >= 200 && response.status < 300) {
    return responseJson;
  } else if (responseJson && responseJson.Message) {
    throw responseJson.Message;
  }
  throw response.statusText || response.status;
}

export function profilePhotoUrl(id) {
  return `${baseURL}api/system/getphoto/${id}`;
}

export async function signIn(username, password) {
  return await prismFetch('api/system/signin', {
    headers: {
      'Authorization': "Basic " + Base64.encode(JSON.stringify({ username, password, persistent: false }))
    }
  });
}

export async function signOut() {
  return await prismFetch('api/system/signout');
}

export async function userInfo() {
  return await prismFetch('api/system/loggedinfo');
}

export async function fetchAllEmployees() {
  return await prismFetch('api/employees/all');
}