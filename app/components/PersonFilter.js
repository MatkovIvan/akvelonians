import React, { Component } from 'react'
import { Platform } from 'react-native'
import { SearchBar } from 'react-native-elements'
import PropTypes from 'prop-types'
import Locales from '../locales'

class PersonFilter extends Component {
  render() {
    const platform = Platform.select({
      ios: 'ios',
      android: 'android',
      default: 'default'
    });
    return <SearchBar
      onChangeText={this.props.onFilterChange}
      platform={platform}
      placeholder={Locales.t('people.search')} />;
  }
}

PersonFilter.propTypes = {
  onFilterChange: PropTypes.func.isRequired
}

export default PersonFilter;
