import React, { Component } from 'react'
import { FlatList } from 'react-native'
import { List } from 'react-native-elements'
import PropTypes from 'prop-types'
import Person from './Person'

class PersonList extends Component {
  renderItem = ({ item }) => {
    let status;
    let onTeamAction;
    if (this.props.me === item.Id) {
      status = 'me';
    } else if (this.props.team.indexOf(item.Id) !== -1) {
      status = 'teammate';
      onTeamAction = this.props.onRemoveFromTeam;
    } else {
      onTeamAction = this.props.onAddToTeam;
    }
    return <Person
      profile={item}
      status={status}
      onTeamAction={onTeamAction} />;
  }

  keyExtractor = (item, index) => item.Id.toString();

  render() {
    return (
      <List containerStyle={{ marginTop: 0 }}>
        <FlatList
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
          data={this.props.people}
          extraData={this.props.team} />
      </List>
    );
  }
}

PersonList.propTypes = {
  people: PropTypes.array.isRequired,
  team: PropTypes.array.isRequired,
  me: PropTypes.number.isRequired,
  onAddToTeam: PropTypes.func.isRequired,
  onRemoveFromTeam: PropTypes.func.isRequired
}

export default PersonList
