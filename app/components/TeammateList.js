import React, { Component } from 'react'
import { FlatList, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'
import Teammate from './Teammate'

class TeammateList extends Component {
  renderItem = ({ item }) => {
    const profile = this.props.people.find(element => element.Id === item);
    return <Teammate
      profile={profile}
      onRemoveFromTeam={this.props.onRemoveFromTeam} />;
  }

  keyExtractor = (item, index) => item.toString();

  render() {
    return <FlatList
      numColumns={2}
      contentContainerStyle={styles.list}
      renderItem={this.renderItem}
      keyExtractor={this.keyExtractor}
      data={this.props.team} />;
  }
}

const styles = StyleSheet.create({
  list: {
    padding: 5
  }
})

TeammateList.propTypes = {
  people: PropTypes.array.isRequired,
  team: PropTypes.array.isRequired,
  onRemoveFromTeam: PropTypes.func.isRequired
}

export default TeammateList
