import React, { Component } from 'react'
import { Image, View, Text, StyleSheet } from 'react-native'
import { Icon } from 'react-native-elements'
import PropTypes from 'prop-types'
import * as Prism from '../services/Prism'

class Teammate extends Component {
  onPressRemoveIcon = () => {
    const { Id } = this.props.profile;
    this.props.onRemoveFromTeam(Id);
  }

  render() {
    const { Id, FirstName, LastName } = this.props.profile;
    const photoUrl = Prism.profilePhotoUrl(Id);
    return (
      <View style={styles.wrapper}>
        <View style={styles.container}>
          <Image source={{ uri: photoUrl }} style={[StyleSheet.absoluteFill, styles.avatar]} />
          <Icon type='material' name='remove-circle' color='red' raised containerStyle={styles.removeIcon}
            onPress={this.onPressRemoveIcon} />
          <Text style={styles.text}>{`${LastName} ${FirstName}`}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    width: '50%',
    aspectRatio: 1,
    padding: 5
  },
  container: {
    width: '100%',
    height: '100%'
  },
  avatar: {
    resizeMode: 'cover'
  },
  removeIcon: {
    width: 25,
    height: 25,
    position: 'absolute',
    right: -10,
    top: -10
  },
  text: {
    position: 'absolute',
    right: -2,
    bottom: -2,
    backgroundColor: 'white',
    paddingHorizontal: 5,
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 5
  }
})

Teammate.propTypes = {
  profile: PropTypes.object.isRequired,
  onRemoveFromTeam: PropTypes.func.isRequired
}

export default Teammate
