import React, { Component } from 'react'
import { View, StyleSheet } from 'react-native'
import { Button, Icon } from 'react-native-elements'
import PropTypes from 'prop-types'
import Locales from '../locales'

export default class LogoutButton extends Component {
  render() {
    return (
      <View style={styles.container} >
        <Button
          text={Locales.t('main.logout')}
          onPress={this.props.onClick}
          icon={<Icon type='entypo' name='log-out' size={20} color={buttonColor} />}
          iconRight
          textStyle={styles.buttonText}
          clear />
      </View>
    );
  }
}

const buttonColor = '#3478f6'
const styles = StyleSheet.create({
  container: {
    marginRight: 10
  },
  buttonText: {
    color: buttonColor,
    paddingRight: 0
  }
})

LogoutButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired
}
