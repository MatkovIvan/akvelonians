import React, { Component } from 'react'
import { View } from 'react-native'
import { ListItem } from 'react-native-elements'
import PropTypes from 'prop-types'
import * as Prism from '../services/Prism'

class Person extends Component {
  onPressRightIcon = () => {
    const { Id } = this.props.profile;
    if (this.props.onTeamAction) {
      this.props.onTeamAction(Id);
    }
  }

  rightIcon = () => {
    switch (this.props.status) {
      case 'teammate': return { name: 'remove-circle', color: 'red' };
      case 'me': return <View />;
      default: return { name: 'add-circle', color: 'green' };
    }
  }

  render() {
    const { Id, FirstName, LastName, Mail } = this.props.profile;
    const photoUrl = Prism.profilePhotoUrl(Id);
    return <ListItem
      roundAvatar
      avatar={{ uri: photoUrl }}
      title={`${LastName} ${FirstName}`}
      subtitle={Mail}
      rightIcon={this.rightIcon()}
      onPressRightIcon={this.onPressRightIcon} />;
  }
}

Person.propTypes = {
  profile: PropTypes.object.isRequired,
  status: PropTypes.string,
  onTeamAction: PropTypes.func,
}

export default Person
