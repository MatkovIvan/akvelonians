export default {
  login: {
    username: "Username",
    password: "Password",
    login: "Login"
  },
  main: {
    logout: "Logout"
  },
  home: {
    title: "Home",
    hello: "Hello, {{name}}!"
  },
  people: {
    title: "People",
    search: "Search"
  },
  team: {
    title: "Team",
    empty: "There are no people in your team..."
  }
};