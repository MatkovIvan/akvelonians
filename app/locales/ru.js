export default {
  login: {
    username: "Имя",
    password: "Пароль",
    login: "Войти"
  },
  main: {
    logout: "Выйти"
  },
  home: {
    title: "Главная",
    hello: "Привет, {{name}}!"
  },
  people: {
    title: "Люди",
    search: "Поиск"
  },
  team: {
    title: "Команда",
    empty: "В твоей команде никого нет..."
  }
};