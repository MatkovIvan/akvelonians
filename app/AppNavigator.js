import React from 'react'
import { View } from 'react-native'
import { NavigationActions, StackNavigator, TabBarBottom, TabNavigator, addNavigationHelpers } from 'react-navigation'
import { connect } from 'react-redux';
import { userLogout } from './actions'
import { HomeScreen, LoginScreen, PeopleScreen, TeamScreen } from './screens'
import { addListener } from './helpers/redux'
import LogoutButton from './components/LogoutButton'

const MainHeaderRight = connect(state => ({
  user: state.user
}), null, ({ user }, { dispatch }, props) => ({
  ...props,
  onClick: () => {
    if (user) {
      dispatch(userLogout());
    }
  }
}))(LogoutButton)

const MainNavigator = TabNavigator(
  {
    Home: { screen: HomeScreen },
    People: { screen: PeopleScreen },
    Team: { screen: TeamScreen }
  },
  {
    initialRouteName: 'Home',
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    navigationOptions: ({ navigation }) => {
      return {
        headerLeft: <View />,
        headerRight: <MainHeaderRight navigation={navigation} />,
        headerTitleStyle: {
          alignSelf: 'center'
        }
      }
    },
  }
)

export const AppNavigator = StackNavigator(
  {
    Login: { screen: LoginScreen },
    Main: { screen: MainNavigator }
  },
  {
    initialRouteName: 'Login',
    mode: 'modal'
  }
)

// Prevent back actions.
const defaultGetStateForAction = AppNavigator.router.getStateForAction;
AppNavigator.router.getStateForAction = (action, state) => {
  if (state && action.type === NavigationActions.BACK) {
    return null;
  }
  return defaultGetStateForAction(action, state);
}

export default connect(state => ({
  navigation: state.navigation
}))(({ dispatch, navigation }) => (
  <AppNavigator navigation= {addNavigationHelpers({
    dispatch,
    state: navigation,
    addListener
  })} />
))
