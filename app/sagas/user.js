import { NavigationActions } from 'react-navigation'
import { call, put } from 'redux-saga/effects'
import * as Prism from '../services/Prism'
import {
  USER_LOGIN_SUCCEEDED,
  USER_LOGIN_FAILED,
  USER_PROFILE_UPDATED,
  USER_LOGOUT
} from '../actions'

export function* userLogin(action) {
  try {
    const user = yield call(Prism.signIn, action.data.username, action.data.password);
    yield put({ type: USER_LOGIN_SUCCEEDED, user: { token: user.Token }});
    yield put(NavigationActions.navigate({ routeName: 'Main' }));
    const profile = yield call(Prism.userInfo);
    yield put({ type: USER_PROFILE_UPDATED, profile });
  } catch (e) {
    console.log('Login error: ', e.message || e);
    yield put({ type: USER_LOGIN_FAILED, message: e.message || e });
  }
}

export function* userLogout(action) {
  yield put(NavigationActions.navigate({ routeName: 'Login' }));
  yield call(Prism.signOut);
}
