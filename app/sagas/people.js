import { call, put } from 'redux-saga/effects'
import * as Prism from '../services/Prism'
import {
  PEOPLE_FETCH_SUCCEEDED,
  PEOPLE_FETCH_FAILED
} from '../actions'

export function* peopleFetch(action) {
  try {
    const people = yield call(Prism.fetchAllEmployees);
    yield put({ type: PEOPLE_FETCH_SUCCEEDED, people });
  } catch (e) {
    yield put({ type: PEOPLE_FETCH_FAILED, message: e.message });
  }
}
