import { takeLatest } from 'redux-saga/effects'
import {
  PEOPLE_FETCH_REQUESTED,
  USER_LOGIN_REQUESTED,
  USER_LOGOUT
} from '../actions'
import { peopleFetch } from './people'
import { userLogin, userLogout } from './user'

export default function*() {
  yield takeLatest(PEOPLE_FETCH_REQUESTED, peopleFetch);
  yield takeLatest(USER_LOGIN_REQUESTED, userLogin);
  yield takeLatest(USER_LOGOUT, userLogout);
}
