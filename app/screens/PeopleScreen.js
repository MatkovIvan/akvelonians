import React, { Component } from 'react'
import { View } from 'react-native'
import { Icon } from 'react-native-elements'
import { createFilter } from 'react-native-search-filter';
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { peopleFetch, teamAddMember, teamRemoveMember } from '../actions'
import PersonFilter from '../components/PersonFilter'
import PersonList from '../components/PersonList'
import Locales from '../locales'

class PeopleScreen extends Component {
  static navigationOptions = {
    title: Locales.t('people.title'),
    tabBarLabel: Locales.t('people.title'),
    tabBarIcon: ({ focused, tintColor }) => {
      return <Icon type='entypo' name='users' color={tintColor} />;
    }
  }

  state = {
    filter: '',
    people: null
  }

  constructor(props) {
    super(props);
    const people = props.people
      .sort(this.compare);
    this.state.people = people;
  }

  compare = (a, b) => a.LastName.localeCompare(b.LastName) || a.FirstName.localeCompare(b.FirstName)
  filter = filter => createFilter(filter, [ 'FirstName', 'LastName', 'Mail' ])

  onFilterChange = filter => {
    const people = this.props.people
      .filter(this.filter(filter))
      .sort(this.compare);
    this.setState({ filter, people });
  }

  componentDidMount() {
    if (this.props.people.length == 0) {
      this.props.peopleFetch();
    }
  }

  componentWillReceiveProps(nextProps) {
    const { filter } = this.state;
    const people = nextProps.people
      .filter(this.filter(filter))
      .sort(this.compare);
    this.setState({ people });
  }

  render() {
    const { profile } = this.props.user || {};
    return (
      <View>
        <PersonFilter onFilterChange={this.onFilterChange} />
        <PersonList
          people={this.state.people}
          team={this.props.team}
          me={profile ? profile.Id : 0}
          onAddToTeam={this.props.teamAddMember}
          onRemoveFromTeam={this.props.teamRemoveMember} />
      </View>
    );
  }
}

PeopleScreen.propTypes = {
  user: PropTypes.object,
  people: PropTypes.array.isRequired,
  team: PropTypes.array.isRequired,
  peopleFetch: PropTypes.func.isRequired,
  teamAddMember: PropTypes.func.isRequired,
  teamRemoveMember: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  user: state.user,
  people: state.people,
  team: state.team
})
const mapDispatchToProps = {
  peopleFetch,
  teamAddMember,
  teamRemoveMember
}
export default connect(mapStateToProps, mapDispatchToProps)(PeopleScreen)
