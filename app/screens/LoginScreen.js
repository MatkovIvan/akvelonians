import React, { Component } from 'react'
import { Image, View, Keyboard, StyleSheet } from 'react-native'
import { Button, Icon, Input } from 'react-native-elements'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { userLogin } from '../actions'
import Locales from '../locales'

import AkvelonLogo from '../assets/akvelon_logo.png'

class LoginScreen extends Component {
  static navigationOptions = {
    header: null
  }

  state = {
    username: '',
    password: ''
  }

  onUsernameChange = username => {
    this.setState({ username });
  }

  onPasswordChange = password => {
    this.setState({ password });
  }

  onLoginClick = () => {
    Keyboard.dismiss();
    const { loading } = this.props.user || {};
    if (loading) {
      return;
    }
    const { username, password } = this.state;
    this.props.userLogin(username, password);
  }

  render() {
    const { error, loading } = this.props.user || {};
    return (
      <View style={styles.container}>
        <Image
          source={AkvelonLogo}
          style={styles.logo} />
        <Input
          placeholder={Locales.t('login.username')}
          onChangeText={this.onUsernameChange}
          leftIcon={<Icon type='entypo' name='user' size={20} />}
          containerStyle={styles.input}
          autoCorrect={false} />
        <Input
          placeholder={Locales.t('login.password')}
          onChangeText={this.onPasswordChange}
          leftIcon={<Icon type='entypo' name='lock' size={20} />}
          containerStyle={styles.input}
          displayError={!!error}
          errorStyle={styles.error}
          errorMessage={error ? error.message : ''}
          secureTextEntry />
        <Button
          text={Locales.t('login.login')}
          onPress={this.onLoginClick}
          containerStyle={styles.buttonContainer}
          buttonStyle={styles.button}
          loading={!!loading} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'center',
    margin: 20
  },
  logo: {
    width: undefined,
    height: 100,
    resizeMode: 'contain'
  },
  input: {
    width: undefined,
    height: 40,
    marginVertical: 5,
    borderWidth: 1,
    borderRadius: 5,
    backgroundColor: 'white'
  },
  error: {
    fontWeight: 'bold',
    color: 'red'
  },
  buttonContainer: {
    marginTop: 10,
    borderRadius: 0,
    alignItems: 'stretch'
  },
  button: {
    height: 40,
    borderRadius: 5
  }
})

LoginScreen.propTypes = {
  userLogin: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  user: state.user
})
const mapDispatchToProps = {
  userLogin
}
export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)
