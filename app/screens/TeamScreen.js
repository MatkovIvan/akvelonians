import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { Badge, Icon } from 'react-native-elements'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { teamRemoveMember } from '../actions'
import TeammateList from '../components/TeammateList'
import Locales from '../locales'

const TeamIcon = connect(state => ({
  count: state.team.length
}))(({ color, count }) => (
  <View style={styles.icon}>
    <Icon type='entypo' name='briefcase' color={color} />
    {count > 0 &&
      <Badge
        wrapperStyle={styles.badgeWrapper}
        containerStyle={styles.badgeContainer}
        textStyle={styles.badgeText}
        value={count} />
    }
  </View>
))

class TeamScreen extends Component {
  static navigationOptions = {
    title: Locales.t('team.title'),
    tabBarLabel: Locales.t('team.title'),
    tabBarIcon: ({ focused, tintColor }) => {
      return <TeamIcon color={tintColor} />;
    }
  }

  render() {
    if (this.props.team.length == 0) {
      return (
        <View style={styles.container}>
          <Text style={styles.text}>{Locales.t('team.empty')}</Text>
        </View>
      );
    }
    return <TeammateList
      people={this.props.people}
      team={this.props.team}
      onRemoveFromTeam={this.props.teamRemoveMember} />;
  }
}

const styles = StyleSheet.create({
  icon: {
    width: 26,
    height: 26
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {

  },
  badgeWrapper: {
    position: 'absolute',
    top: -2,
    right: -8,
  },
  badgeContainer: {
    backgroundColor: 'red',
    paddingHorizontal: 6,
    paddingTop: 2,
    paddingBottom: 2
  },
  badgeText: {
    fontSize: 8
  }
})

TeamScreen.propTypes = {
  people: PropTypes.array.isRequired,
  team: PropTypes.array.isRequired,
  teamRemoveMember: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  people: state.people,
  team: state.team
})
const mapDispatchToProps = {
  teamRemoveMember
}
export default connect(mapStateToProps, mapDispatchToProps)(TeamScreen)
