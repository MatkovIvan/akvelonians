import React, { Component } from 'react'
import { ActivityIndicator, Image, View, Text, StyleSheet } from 'react-native'
import { Icon } from 'react-native-elements'
import { connect } from 'react-redux'
import Locales from '../locales'
import * as Prism from '../services/Prism'

class HomeScreen extends Component {
  static navigationOptions = {
    title: Locales.t('home.title'),
    tabBarLabel: Locales.t('home.title'),
    tabBarIcon: ({ focused, tintColor }) => {
      return <Icon type='entypo' name='home' color={tintColor} />;
    }
  }

  render() {
    if (!this.props.user || !this.props.user.profile) {
      return (
        <View style={styles.container}>
          <ActivityIndicator />
        </View>
      );
    }
    const { Id, FirstName, LastName } = this.props.user.profile;
    const photoUrl = Prism.profilePhotoUrl(Id);
    return (
      <View style={styles.container}>
        <Image source={{ uri: photoUrl }} style={styles.photo} />
        <Text style={styles.text}>{Locales.t('home.hello', { name: `${LastName} ${FirstName}` })}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  photo: {
    width: 100,
    height: 100,
    margin: 20,
    borderRadius: 10
  },
  text: {
    fontWeight: 'bold',
    fontSize: 26
  }
})

const mapStateToProps = state => ({
  user: state.user
})
export default connect(mapStateToProps)(HomeScreen)
