const Languages = jest.mock('react-native-languages');
Languages.language = 'en';
Languages.languages = ['en'];
export default Languages;
